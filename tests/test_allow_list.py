"""
Tests for the allow list functionality.
"""

from src.allow_list import is_url_allowed

def test_is_url_allowed_false():
    """
    Test with a URL that's not part of the allow list.
    """
    url = 'https://google.com'
    result = is_url_allowed(url)
    assert result is False

def test_is_url_allowed_true():
    """
    Test with a URL that's part of the allow list.
    """
    url = 'https://ahikar-dev.sub.uni-goettingen.de/api/textapi/ahiqar/arabic-karshuni/3r17c/147a/latest/item.json'
    result = is_url_allowed(url)
    assert result is True
