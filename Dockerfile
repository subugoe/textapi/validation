FROM docker.io/python:3.11-slim

WORKDIR /app
COPY requirements.txt /app/ 
RUN pip3 install -r requirements.txt

COPY ./src /app
COPY ./schemas/ /app/schemas/
COPY ./allow_list.json /app/


EXPOSE 8000

CMD ["uvicorn", "api:app", "--host=0.0.0.0", "--reload"]