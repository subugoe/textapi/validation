"""
A small API for validating your TextAPI implementation against the latest TextAPI version.
"""

import json
import os
from typing import Annotated

import jsonschema
import requests
from fastapi import FastAPI, Query, Response, status
from fastapi.responses import JSONResponse, Response
from jsonschema import exceptions

from requests.adapters import HTTPAdapter, Retry

from allow_list import is_url_allowed
from metadata import API, PARAM, POST

import re

import sentry_sdk

sentry_sdk.init(
    dsn="https://d43c45d7868bd42c47ac32cc267d2321@errs.sub.uni-goettingen.de/3",
    traces_sample_rate=1.0,
    release=f"{API['title']}@{API['version']}"
)

app = FastAPI(title=API['title'],
              description=API['desc'],
              version=API['version'],
              contact=API['contact'],
              license_info=API['license'],)

@app.get("/healthcheck", responses={204: {"model": None}})
async def healthcheck():
    return Response(status_code=status.HTTP_204_NO_CONTENT)

@app.post("/", responses=POST['responses'], summary=POST['summary'])
async def validate_rest(response: Response,
                        url: Annotated[str, Query(
                            title=PARAM['url_title'], description=PARAM['url_desc'])],
                        recursive: Annotated[bool,
                            Query(title=PARAM['rec_title'], description=PARAM['rec_desc'])]):
    """
    The REST end point that accepts URLs to validate.
    Usage e.g.:

    ```
        curl -X 'POST' \\
        'http://127.0.0.1:8000/?url=https%3A%2F%2Fahikar.sub.uni-goettingen.de%2Fapi%2Ftextapi%2Fahikar%2Fsyriac%2Fcollection.json&recursive=false' \\
        -H 'accept: application/json' \\
        -d ''
    ```

    URLs have to conform to the [TextAPI delivery service specs
    ](https://subugoe.pages.gwdg.de/emo/text-api/page/specs/#delivery-service).
    """
    if is_url_allowed(url):
        results = validate(url, recursive)
        validation_errors = filter_errors(results)
        if not validation_errors:
            response.status_code = status.HTTP_200_OK
            return JSONResponse(status_code=200, content={"message": "Success"})
        else:
            response.status_code = status.HTTP_422_UNPROCESSABLE_ENTITY
            return validation_errors
    else:
        response.status_code = status.HTTP_403_FORBIDDEN
        return JSONResponse(status_code=403, content={"message": "Unauthorized URL"})


def filter_errors(results: list) -> list:
    """
    Removes all dictionaries from the result list that have an empty
    value (i.e. that represent valid files).
    """
    filtered = []
    for res in results:
        if isinstance(list(res.values())[0], dict):
            filtered.append(res)
    return filtered

def validate(url: str, recursive: bool):
    """
    A wrapper function to distinguish between recursive and simple validation.
    """
    if recursive:
        result = flatten_recursive_result(validate_recursively(url), [])
    else:
        result = [validate_single_file_from_url(url)]
    return result


def validate_recursively(url: str) -> list:
    """
    Recursive validation.
    Checks if a Sequence Object is available. If so, validate current object and everything
    in sequence.
    """
    results = []
    data = get_json_from_url(url)
    results.append(validate_single_file_from_data(data, url))
    try:
        if 'sequence' in data.keys():
            for element in data['sequence']:
                results.append(validate_recursively(element['id']))
    except AttributeError:
        res = {url: data[1]}
        results.append(res)
    return results

def flatten_recursive_result(results: list, flattened: list) -> list:
    """
    The recursion result can have up to three levels (one for collection, manuscript, and item,
    respectively).
    For easier processing the result should be a flat list, though.
    """
    for entry in results:
        if isinstance(entry, list):
            flatten_recursive_result(entry, flattened)
        else:
            flattened.append(entry)
    return flattened


def validate_single_file_from_url(url: str) -> dict:
    """
    A wrapper function to retrieve data and execute the validation.
    """
    data = get_json_from_url(url)
    return validate_single_file_from_data(data, url)


def validate_single_file_from_data(data: dict, url: str) -> dict:
    """
    The actual validation using the schema given at schemas/textapi/.
    """
    api_type = determine_api_type(url)
    entity_type = determine_type(url)
    schema = get_schema(entity_type, api_type)
    this_dir = os.path.dirname(os.path.realpath(__name__))
    refres = jsonschema.RefResolver(
        referrer=schema, base_uri=f'file://{this_dir}/schemas/{api_type}/')

    try:
        return {url: jsonschema.validate(data, schema, resolver=refres)}
    except exceptions.ValidationError as error:
        return {url: {'error': error.message,
            'instance': error.instance,
            'path': error.absolute_path
            }}


def determine_api_type(url: str) -> str:
    pattern = re.compile('annotation(Collection|Page).json')
    if pattern.search(url) is None:
        return "textapi"
    else:
        return "annotationapi"

def get_json_from_url(url: str) -> dict:
    """
    Downloads a JSON given by a URL.
    """
    header = {"Accept": "application/json"}
    s = requests.Session()
    retries = Retry(total=5, backoff_factor=1, status_forcelist=[ 502, 503, 504 ])
    s.mount('http://', HTTPAdapter(max_retries=retries))

    response = s.get(url, headers=header, timeout=10)
    #response = requests.get(url, headers=header, timeout=10)
    return json.loads(response.text)


def determine_type(url: str) -> str:
    """
    Returns the entity type (collection, manifest, item, 
    annotationCollection, annotationPage) of a given URL.
    We use the fact that the last part of the TextAPI has always to be 
    (collection|manifest|item|annotationCollection|annotationPage).json.
    """
    return url.split('/')[-1].split('.')[0]


def get_schema(entity_type: str, api_type: str) -> dict:
    """
    Returns a dictionary containing a schema as JSON
    """
    file_path = f'schemas/{api_type}/{entity_type}.json'
    try:
        with open(file_path, 'r', encoding='utf-8') as jsondata:
            return json.load(jsondata)
    except:
        print("File not found: " + file_path)
