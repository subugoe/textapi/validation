"""
Checks if a given URL is part of the allow list.
New projects have to registered in allow_list.json.
"""

import json

def is_url_allowed(url: str) -> bool:
    """
    Checks if a given URL is part of the allow list.
    """
    with open('allow_list.json', mode='r', encoding='utf-8') as allow_list_data:
        allow_list = json.loads(allow_list_data.read())
        flag = False
        for entry in allow_list:
            if entry in url:
                flag = True
                break
        return flag
