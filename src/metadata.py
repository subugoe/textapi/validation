"""
Metadata for OpenAPI
"""

__all__ = [
    'API',
    'PARAM',
    'POST'
]

API = {
    'title': 'TextAPI Validation Service',
    'desc': 'A small API for validating your TextAPI implementation against the latest TextAPI version.',
    'version': '0.0.1',
    'contact': {
        "name": "TextAPI at SUB Göttingen",
        "url": "https://textapi.sub.uni-goettingen.de/contact",
        "email": "textapi@sub.uni-goettingen.de",
    },
    'license': {
        "name": "EU-PL v1.2",
        "url": "https://joinup.ec.europa.eu/sites/default/files/custom-page/attachment/2020-03/EUPL-1.2%20EN.txt",
    }
}

PARAM = {
    'url_title': 'TextAPI URL',
    'url_desc': 'An URL pointing to a TextAPI Collection/Manifest/Item Object',
    'rec_title': 'Recursion flag',
    'rec_desc': 'Flag to indicate if all URLs given in a Sequence Object should be validated as well'
}

POST = {
    'summary': 'Validate a TextAPI implementation given at a URL',
    'responses': {
        200: {
            'description': '**OK**. Successful validation',
            'content': {
                'application/json': {
                    'example': 
                        {'message': 'Success'}
                }
            }
        },
        403: {
            'description': '**FORBIDDEN**. URL posted not registered in allow list',
            'content': {
                'application/json': {
                    'example': 
                        {'message': 'Unauthorized URL'}
                }
            }
        },
        422: {
            'description': '**Unprocessable Entity**. Error in POST parameter(s)',
            'content': {
                'application/json': {
                    'example': 
                        {"detail": [
                            {
                            "loc": [
                                "string",
                                0
                            ],
                            "msg": "string",
                            "type": "string"
                            }
                        ]}
                }
            }
        },
        500:
            {
                'description': '**Internal Server Error.** \
                    This occurs when the validation fails.\
                    Attention: Validation errors on root level result in an empty `path` \
                    property as can be seen in the example.',
                'content': {
                    'application/json': {
                        'example':
                            [
                            {
                                "https://ahikar.sub.uni-goettingen.de/api/textapi/ahikar/syriac/collection.json": {
                                "error": "'@context' is a required property",
                                "instance": {
                                    "textapi": "1.0.0",
                                    "x-app-version": {
                                    "version": "7.8.1"
                                    },
                                    "title": [
                                    {
                                        "title": "Textual witnesses in Syriac",
                                        "type": "main"
                                    }
                                    ],
                                    "collector": [
                                    {
                                        "role": [
                                        "collector"
                                        ],
                                        "name": "Prof. Dr. theol. Kratz, Reinhard Gregor",
                                        "idref": {
                                        "base": "http://d-nb.info/gnd/",
                                        "id": "115412700",
                                        "type": "GND"
                                        }
                                    },
                                    {
                                        "role": [
                                        "collector"
                                        ],
                                        "name": "Birol, Simon",
                                        "idref": {
                                        "base": "http://d-nb.info/gnd/",
                                        "id": "1150408537",
                                        "type": "GND"
                                        }
                                    }
                                    ],
                                    "description": "Syriac collection for the Ahiqar project. Funded by DFG, 2018–2021, University of Göttingen",
                                    "annotationCollection": "https://ahiqar.uni-goettingen.de/api/annotations/ahikar/syriac/annotationCollection.json",
                                    "sequence": [
                                    {
                                        "id": "https://ahiqar.uni-goettingen.de/api/textapi/ahikar/syriac/3r678/manifest.json",
                                        "type": "manifest"
                                    }
                                    ]
                                },
                                "path": []
                                }
                            }
                            ]
                    }
                }
            }
    }
}
