# TextAPI Validation Service

A small API for validating your TextAPI implementation against the latest TextAPI version.

The service will be available at [https://validation.textapi.sub.uni-goettingen.de](https://validation.textapi.sub.uni-goettingen.de).

## Requirements

- Python 3.10.9 or higher

## Local Installation

The TextAPI Validation Service runs in the SUB's Kubernetes cluster, but can be fired up locally nevertheless.

To get the service up and running on your machine proceed as follows:

```bash
# clone the repo
git clone git@gitlab.gwdg.de:subugoe/textapi/validation.git && cd validation
# create a venv
python3 -m venv venv
source venv/bin/activate
# install the dependencies
pip install -r requirements.txt
# start the webserver
uvicorn api:app --reload --app-dir "$PWD"/src
```

The API is now available at localhost:8000.

## Usage

The TextAPI Validation Service is designed to accept an URL conforming to the [TextAPI's delivery service](https://subugoe.pages.gwdg.de/emo/text-api/page/specs/#delivery-service).
**URLs have to be registered in our allow list in order to be validated.**

Detailed information on the API can be found in the Validation Service's [API documentation](https://validation.textapi.sub.uni-goettingen.de/docs).

## Support

Feel free to open a ticket in our [issue board](https://gitlab.gwdg.de/subugoe/textapi/validation/-/issues).

## Roadmap

- implement a white list for security reasons
- add a front end with a form where users can post either a URL or a part of their TextAPI
- make validation specific to TextAPI version provided in the data (instead of using the latest schema version)

## Contributing

We welcome any contributions in form of feature requests or code improvements.
Simply open a MR and request a review from either [Mathias Göbel](https://gitlab.gwdg.de/mgoebel) or [Michelle Weidling](https://gitlab.gwdg.de/mrodzis).

## Authors and acknowledgment

See [Contributors](https://gitlab.gwdg.de/subugoe/textapi/validation/-/graphs/main?ref_type=heads).

## License

[EU-PL v1.2](LICENSE)
